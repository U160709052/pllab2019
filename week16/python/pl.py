import time
import sys
def sorting(unsorted):
       newDict={}
       for item,item2 in unsorted.items():
              for i in item2.items():
                     newDict[item]={'Grades':i[0]}
       sorted_keys = sorted(newDict, key=lambda x: (newDict[x]['Grades']))
       newDict.clear()
       for key in sorted_keys:
              newDict[key]=unsorted.get(key)
       return newDict


gradeFile=open(sys.argv[1], "r")

hashes={}
# +++ read file and adding item on hashes +++
for line in gradeFile:
    lineList=line.split()
    hashes[str(lineList[0])]={str(lineList[1]):""}
# --- read file and adding item on hashes ---

# +++ Sorting +++
start_time = time.time()
sortedDict=sorting(hashes)
for name,grades in sortedDict.items():
       for i in grades.items():
              print(name,"=",i[0])
print("--- %s seconds for sorting and printing ---" % (time.time() - start_time))
# --- Sorting ---

# +++ Deleting +++
input=input("Which item has to be deleted = ")
start_time = time.time()
try:
       del sortedDict[input]
except:
       print("There is no ",input)
print("--- %s seconds for deleting ---" % (time.time() - start_time))
# --- Deleting ---