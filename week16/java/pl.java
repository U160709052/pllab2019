import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
public class pl {


    public static HashMap<String, HashMap<String, String>> sortByValue(HashMap<String, HashMap<String, String>> hm) 
    { 
        HashMap<String, String> newHm=new HashMap<String, String>();
        for (String keye : hm.keySet() ) {
            for ( String keyi : hm.get(keye).keySet() ) {
                newHm.put(keye, keyi);
            }
        }
        
        // Create a list from elements of HashMap 
        List<Map.Entry<String, String> > list = 
               new LinkedList<Map.Entry<String, String> >(newHm.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<String, String> >() { 
            public int compare(Map.Entry<String, String> o1,  
                               Map.Entry<String, String> o2) 
            { 
                return (o1.getValue()).compareTo(o2.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<String, HashMap<String, String>> temp = new LinkedHashMap<String, HashMap<String, String>>(); 
        for (Map.Entry<String, String> aa : list) { 
            HashMap<String, String> hashA = new HashMap<String, String>();
            hashA.put(aa.getValue(), "");
            temp.put(aa.getKey(), hashA);
        } 

        return temp; 
    } 

    public static void main(String[] args) {
        HashMap<String, HashMap<String, String>> deep = new HashMap<String, HashMap<String, String>>();

        // The name of the file to open.
        String fileName = args[0];

        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // +++READING FILE AND FILL THE HASH+++
            while((line = bufferedReader.readLine()) != null) {
                HashMap<String, String> hashA = new HashMap<String, String>();
                hashA.put(line.split(" ")[1], "");
                deep.put(line.split(" ")[0], hashA);
                //System.out.println(line.split(" ")[0]+" "+line.split(" ")[1]);
            }   
            // ---READING FILE AND FILL THE HASH---

            // Always close files.
            bufferedReader.close();    
            
            // +++SORTING AND PRINTING+++

            Instant start = Instant.now();

            HashMap<String, HashMap<String, String>> sortedMap = sortByValue(deep);
            for (String keye : sortedMap.keySet() ) {
                for ( String keyi : sortedMap.get(keye).keySet() ) {
                    System.out.println(keye +" = "+ keyi);
                }
            } 

            Instant finish = Instant.now();
 
            long timeElapsed = Duration.between(start, finish).toNanos();

            System.out.println(timeElapsed+" nanosecond for sorting and printing ");
            // ---SORTING AND PRINTING---


            // +++DELETING+++

            Scanner myObj = new Scanner(System.in);  // Create a Scanner object
            System.out.println("Which item has to be deleted : ");

            String key = myObj.nextLine();  // Read user input
            myObj.close();
            start = Instant.now();
            sortedMap.remove(key);
            finish = Instant.now();
 
            timeElapsed = Duration.between(start, finish).toNanos();
            System.out.println(timeElapsed+" nanosecond for deleting ");
            // ---DELETING---

        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }
    }
}